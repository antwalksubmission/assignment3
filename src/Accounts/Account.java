package Accounts;

import Customer.Customer;



public class Account {
    String accountId;
    String description;
    Float minimumBalance;
    Customer owner;



    public Account(String accountId, String description, Float minimumBalance, Customer owner) {
        this.accountId = accountId;
        this.description = description;
        this.minimumBalance = minimumBalance;
        this.owner=owner;
    }

    public String getAccountId() {
        return accountId;
    }

        public void display()
    {
    }

}
