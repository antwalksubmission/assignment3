package Accounts;

import Customer.Customer;

public class CurrentAccount extends Account{
    static int count=1;
    public CurrentAccount(Customer owner) {
        super("BS"+count,"Current", 500f,owner);
        count++;
    }

    @Override
    public String toString() {
        return "CurrentAccount{" +
                "accountId='" + accountId + '\'' +
                ", description='" + description + '\'' +
                ", minimumBalance=" + minimumBalance +
                ", owner=" + owner.getName() +
                '}';
    }

    @Override
    public void display()
    {
        System.out.println("The details of the account are"+this);
    }
}
